import './css/App.css';
import Header from './components/Header';
import Main from './components/Main';
import Footer from './components/Footer';
import {useSelector} from 'react-redux';

function App() {
  const fontSize = useSelector(state => state.fontSize);
  return (
    <div className="app" style={{fontSize: fontSize + "%"}}>
      <Header text="APP'S NAME"/>
      <Main/>
      <Footer text="Footer"/>
    </div>
  );
}

export default App;
