const CHANGE_FONT_SIZE = 'CHANGE_FONT_SIZE';
export function changeFontSize(size) {
    return {
        type: CHANGE_FONT_SIZE,
        size,
    }
}

export function increaseSize(size) {
    return {
        type: INCREASE_SIZE,
        size
    }
}

const defaultFont = [
    {
      fontSize: 100,
    }
  ];
  
function font(state=defaultFont, action) {
    switch (action.type) {
        case CHANGE_FONT_SIZE:
        return [
            state,
            {
                fontSize: action.size,
            }
        ]
        case INCREASE_SIZE:
            return state !== 125 ? state + 5 : state
        case INCREASE_SIZE:
                return state !== 75 ? state + 5 : state
        default:
            return state;
    }
}