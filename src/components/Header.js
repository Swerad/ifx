import React from 'react';
import Menu from './Menu';

const Header = ({text}) => {

  return (
    <div className="header">
      <div className="header__title">{text}</div>
      <button className="header__btn"></button>
      <Menu/>
    </div>
  )
}

export default Header