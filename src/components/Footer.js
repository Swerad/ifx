import React from 'react'

const Footer = ({text}) => {
  return (
    <div className="footer">{text}</div>
  )
}

export default Footer