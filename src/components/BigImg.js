import React from 'react';

export const BigImg = () => {
  return (
    <div className="main__bigImg">
      <div 
        className="main__bigImgTextElement"
        >Lorem ipsum</div>
    </div>
  )
}

export default BigImg;
