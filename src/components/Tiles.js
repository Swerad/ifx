import React from 'react';
import TilesItem from './TilesItem';

const Tiles = () => {
  return (
    <div className="main__tiles">
        <TilesItem title="TITLE 1"/>
        <TilesItem title="TITLE 2"/>
        <TilesItem title="TITLE 3"/>
        <TilesItem title="TITLE 4"/>
    </div>
  )
}

export default Tiles