import React from 'react'

const BigTileItem = ({title}) => {
  return (
    <div className="main__bigTileItem">
      <div className="main__bigTileItemImgBox">
        <img 
            className="main__bigTileItemImg"
            src={require('./../img/swit.jpg')}
            alt=""
        />
      </div>
        <div className="main__bigTileItemTitle">{title}</div>
    </div>
  )
}
export default BigTileItem