import React from 'react';
import BigImg from './BigImg';
import Tiles from './Tiles';
import BigTiles from './BigTiles';
import FontDriver from './FontDriver';

const Main = () => {
  return (
    <div className="main">
        <BigImg/>
        <Tiles/>
        <BigTiles/>
        <FontDriver/>
    </div>
  )
}

export default Main