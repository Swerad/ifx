import React from 'react';
import MenuItem from './MenuItem';

const Menu = () => {
  return (
    <div className="header__menu">
        <MenuItem text="ITEM1"/>
        <MenuItem text="ITEM2"/>
        <MenuItem text="ITEM3"/>
    </div>
  )
}

export default Menu