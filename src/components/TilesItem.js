import React from 'react'

const TilesItem = ({title}) => {
  return (
    <div className="main__tileItem">
        <div className="main__tileItemTitle">{title}</div>
        <div className="main__tilesItemImgBox">
          <img className="main__tileItemImg" src={require('./../img/swit.jpg')} alt="dawn"/>
        </div>
        <div className="main__tileItemText">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatem quas provident iure doloremque eaque</div>
        <a href="/" className="main__tileItemLink">link</a>
    </div>
  )
}

export default TilesItem