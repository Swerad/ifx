import React from 'react'

const MenuItem = ({text}) => {
  return (
    <div className="header__menuItem">
      <a className="header__menuItemLink" href="/">{text}</a>
    </div>
  )
}

export default MenuItem