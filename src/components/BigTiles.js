import React from 'react'
import BigTileItem from './BigTileItem';

const BigTiles = () => {
  return (
    <div className="main__bigTiles">
        <BigTileItem title="TITLE 5"/>
        <BigTileItem title="TITLE 6"/>
    </div>
  )
}

export default BigTiles