import React from 'react'

const FontDriver = () => {
  return (
    <div className="fontDriver">
        <div className="fontDriver__fontSizePanel">
            <div className="fontDriver__fontSizePanelTitle">Change font-size</div>
            <div className="fontDriver__fontSizeBtn fontDriver__fontSizeBtn--less">-</div>
            <div className="fontDriver__fontSizeBtn fontDriver__fontSizeBtn--more">+</div>
        </div>    
    </div>
  )
}

export default FontDriver